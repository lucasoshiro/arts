# FLUSP's arts

This repo contains the arts used by FLUSP, in a variety of cases.

### Contributing

'Source' image formats, such as svg or xcf, are prefered over 'final' image
formats such as pngs and jpgs.
